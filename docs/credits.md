# Deep learning resources
## Pytroch 
- https://deeplizard.com/
## Math
- http://colah.github.io/

## Topics need speacial focus 
- Computational graphs 

### Path to pytorch env
- conda activate ~/opt/anaconda3/envs/pytorch

### TDD
[Test Driven Development](https://code.likeagirl.io/in-tests-we-trust-tdd-with-python-af69f47e6932)

[python-3-patterns-idioms-test](https://python-3-patterns-idioms-test.readthedocs.io/en/latest/UnitTesting.html)