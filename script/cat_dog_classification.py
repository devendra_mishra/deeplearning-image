import matplotlib.pyplot as plt
import torch
from torchvision import datasets, transforms

data_dir = "~/Documents/personal_project/image/deeplearning-image/deep-learning-v2-pytorch/intro-to-pytorch/Cat_Dog_data"

transform = transforms.Compose([transforms.Resize(255),
                                transforms.CenterCrop(224),
                                transforms.ToTensor()]) # TODO: compose transforms here
dataset = datasets.ImageFolder(data_dir, transform=transform)# TODO: create the ImageFolder
dataloader = torch.utils.data.DataLoader(dataset, batch_size=32, shuffle=True)# TODO: use the ImageFolder dataset to create the DataLoader

# Run this to test your data loader
images, labels = next(iter(dataloader))
plt.imshow(images[0].view(224,224,3).numpy())
print(images[0].view(224,224,3).numpy().shape)
#helper.imshow(images[0], normalize=False)
#print(images)