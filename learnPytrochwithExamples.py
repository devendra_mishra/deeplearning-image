import torch
import torchvision
import torchvision.transforms as transforms
import torch.nn as nn
import torch.nn.functional as F

class Network(nn.Module): # inheritance 
    def __init__(self):
        super().__init__() # call to the constructor of base class
        
        # conv layers below
        self.conv1 = nn.Conv2d(in_channels=1, out_channels=6, kernel_size=5)
        self.conv2 = nn.Conv2d(in_channels=6, out_channels=12, kernel_size=5)
        
        # fully connected layers below
        self.fc1 = nn.Linear(in_features=12 * 4 * 4, out_features=120)
        self.fc2 = nn.Linear(in_features=120, out_features=60)
        self.out = nn.Linear(in_features=60, out_features=10)

    def forward(self, t):
        
        t = t
        # hidden conv layer 1
        t = self.conv1(t)
        t = F.relu(t)
        t = F.max_pool2d(t, kernel_size=2, stride=2)
        
        # hidden conv layer 2
        t = self.conv2(t)
        t = F.relu(t)
        t = F.max_pool2d(t, kernel_size=2, stride=2)

        # (4) hidden linear layer
        t = t.reshape(-1, 12 * 4 * 4)
        t = self.fc1(t)
        t = F.relu(t)

        # (5) hidden linear layer
        t = self.fc2(t)
        t = F.relu(t)

        # (6) output layer
        t = self.out(t)
        t = F.softmax(t, dim=1)
        return t

if __name__ == "__main__":

    # load data
    train_set = torchvision.datasets.FashionMNIST(
    root='~/Documents/personal_project/image/deeplearning-image/notebook/data/'
    ,train=True
    ,download=False
    ,transform=transforms.Compose([
        transforms.ToTensor()
        ])
    )

    # prepare input 
    image, label = train_set[0]
    print(image.shape)
    image, label = train_set[0]
    # the network expects the input to have batchsize;
    # (batch_size, channels, input_width, input_height); a 4D vector
    test_input = image.unsqueeze(0)

    # instatiate the network
    network = Network()
    output = network.forward(test_input)
    print(output)
    print(output.max())
    